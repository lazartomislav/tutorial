<?php

class Person {
	// Properties
	private $name;
	private $eyecolor;
	private $age;

	public static $drinkingAge = 21;

	public function __construct($name, $eyecolor, $age) {
		$this->name = $name;
		$this->eyecolor = $eyecolor;
		$this->age = $age;
	}

	// Methods
	public function setName ($name) {
		$this->name = $name;
	}

	public function getDA() {
		return self::$drinkingAge;

	}

	public static function setDrinkingAge($newAge) {
		self::$drinkingAge = $newAge;
	}
}
